function $(id) {
	return document.getElementById(id);
}
// WebGL variables
var scene,
	camera,
	renderer,
	geometry,
	material,
	cube;
// enumeration of modes for running simulation
var ProgressMode = {
		Play: "play",
		Stop: "stop"};
// current progress mode the simulation is in
var progressMode;
// amount of rotation to apply each frame
var rotation = new THREE.Vector3(0,0,0);
// counts number of frames that have passed since start of simulation
var frameNum = 0;
	
window.onload = function() {
	// start the simulation playing
	progressMode = ProgressMode.Play;
	$('progressButton').value = "Stop";
	
	// attach event listeners
	$('progressButton').addEventListener("click", toggleProgressMode);
	$('xRotation').addEventListener("change", updateRotation);
	$('yRotation').addEventListener("change", updateRotation);
	$('zRotation').addEventListener("change", updateRotation);
	$('cubeColor').addEventListener("change", updateColor);
	$('colored').addEventListener("change", updateColor);
	$('blackWhite').addEventListener("change",updateColor);
	
	// keylistener
	window.onkeydown = function(e) { keydown(e)};
	
	// get initial rotation values
	updateRotation();
	// initialize webGL
	initialize();
	updateColor();
	console.log("Starting rendering...");
	render();
}

// initialize the webGL scene
function initialize() {
	console.log("Initializing cube...");
	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 0.1, 100);

	renderer = new THREE.WebGLRenderer();
	renderer.setSize( window.innerWidth/2, window.innerHeight/2 );
	document.body.appendChild(renderer.domElement);
	renderer.domElement.id = "canvas";

	box = new THREE.BoxGeometry(1,1,1);
	material = new THREE.MeshBasicMaterial( { color: $('cubeColor').value });
	cube = new THREE.Mesh(box, material);
	scene.add(cube);
	
	// start camera at standoff
	camera.position.z = 2;
}

// render the scene and ask for animation frame
function render() {
	// only run when in play mode
	if (progressMode == ProgressMode.Play) {
		frameNum++;
		// rotate the cube by the given inputs
		cube.rotateX(rotation.getComponent(0));
		cube.rotateY(rotation.getComponent(1));
		cube.rotateZ(rotation.getComponent(2));
		// update the output to show veritce positions
		updatePoints();
		// Request rendering of a frame
		requestAnimationFrame(render);
		renderer.render(scene, camera);
	}
}

// Toggle the progress mode
function toggleProgressMode() {
	if (progressMode != ProgressMode.Play) {
		progressMode = ProgressMode.Play;
		$('progressButton').value = "Stop";
		render();
	}
	else if (progressMode != ProgressMode.Stop) {
		progressMode = ProgressMode.Stop;
		$('progressButton').value = "Start";
	}
	else {
		console.log("Progress mode was not set.");
	}
}

// Base keypress events
function keydown(e) {
	// "W" - 87
	if (e.keyCode == 87) {
		// move camera forward
		if (camera.position.z > 0.3)
			camera.position.z -= 0.3;
	}
	// "A" - 65
	if (e.keyCode == 65) {
		// move camera left
	}
	// "S" - 83
	if (e.keyCode == 83) {
		// move camera back
		if (camera.position.z < 20)
			camera.position.z += 0.3;
	}
	// "D" - 68
	if (e.keyCode == 68) {
		// move camera right
	}
}

// get the rotation amount for the axes
function updateRotation() {
	rotation.setComponent(0, $('xRotation').value);
	rotation.setComponent(1, $('yRotation').value);
	rotation.setComponent(2, $('zRotation').value);
	console.log("Updated rotation to (" + rotation.getComponent(0) + "," + 
		rotation.getComponent(1) + "," + rotation.getComponent(2) + ")");
}

// update the color and style of the cube
function updateColor() {
	var color;
	// set the color used for drawing
	if (!$('blackWhite').checked) {
		// colorful!
		color = $('cubeColor').value;
	}
	else {
		// Black and white
		color = '#ffffff';
	}
	// set the material used for rendering the cube
	if($('colored').checked) {
		// Solid color
		cube.material = new THREE.MeshBasicMaterial( { color: color });
	}
	else {
		// include wireframe with color
		cube.material = new THREE.MeshBasicMaterial( { color: color, wireframe: true} );
	}
}

// update the display for the position of all veritces
function updatePoints() {
	for(var vert = 0; vert < cube.geometry.vertices.length; vert++) {
		$("v" + (vert + 1) + "x").innerHTML = cube.geometry.vertices[vert].x;
		$("v" + (vert + 1) + "y").innerHTML = cube.geometry.vertices[vert].y;
		$("v" + (vert + 1) + "z").innerHTML = cube.geometry.vertices[vert].z;
	}
}